class ShipmentData {
  String userId;
  String countryFrom;
  String countryTo;
  String stateFrom;
  String stateTo;
  DateTime date;
  String description;
  String dimension1;
  String dimension2;
  String dimension3;
  String weight;
  String fragile;
  String imageUrl;
}