class TripData {
  String userId;
  String countryFrom;
  String countryTo;
  String stateFrom;
  String stateTo;
  DateTime date;
  String means;
  String description;
}