import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class CountriesData {
  Future<String> _loadCountriesAsset() async {
    return await rootBundle.loadString('json_data/countries_data.json');
  }

  Future<List<String>> getCountries() async {
    String jsonCountries = await _loadCountriesAsset();
    return _parseJsonForCountries(jsonCountries);
  }

  Future<List<String>> getStates({countryName}) async {
    String jsonCountries = await _loadCountriesAsset();
    return _parseJsonForStates(jsonCountries, countryName);
  }

  List<String> _parseJsonForCountries(String jsonString) {
    Map decoded = jsonDecode(jsonString);
    List countries = decoded['Countries'];

    List<String> countriesNames = [];
    countries.forEach((country) {
      countriesNames.add(country['CountryName']);
    });

    return countriesNames;
  }

  List<String> _parseJsonForStates(String jsonString, String countryName) {
    Map decoded = jsonDecode(jsonString);
    List countries = decoded['Countries'];

    List<String> statesNames = [];
    countries.forEach((country) {
      if (country['CountryName'] == countryName) {
        print('I was here one day');
        List states = country['States'];
        states.forEach((state) {
          statesNames.add(
            state['StateName']
          );
        });
      }
    });
    return statesNames;
  }
}
