class Validator {
  static String validateEmail(String value) {
    Pattern pattern = r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Please enter a valid email address.';
    else
      return null;
  }

  static String validatePassword(String value) {
    Pattern pattern = r'^.{6,}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Password must be at least 6 characters.';
    else
      return null;
  }

  static String validateName(String value) {
    Pattern pattern = r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Please enter a name.';
    else
      return null;
  }

  //
  static String validateDropdownMenu(String value) {
    if (value == null) {
      return 'Select Value.';
    } else {
      return null;
    }
  }

  static String validateDescription(String value) {
    Pattern pattern = r'^.{15,}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Description must be at least 15 characters.';
    else {
      return null;
    }
  }

  static String validateLocation(String value) {
    if (value.isEmpty) {
      return 'Please determine your location.';
    } else {
      return null;
    }
  }

  //
  static String validateNumber(String value) {
    // Pattern pattern = r'^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$';
    // RegExp regex = new RegExp(pattern);
    // if (!regex.hasMatch(value))
    //   return 'Missed';
    if(value.isEmpty || num.tryParse(value) == null || num.parse(value) <= 0)
      return 'Missed';
    else
      return null;
  }

  //TODO: make this later.
  static String validatePhone(String value) {
    return null;
  }
}
