class DateFilter {
  static bool compareDatesWithRangeAndNow({
    DateTime sourceDate,
    DateTime destinationDate,
    int daysRange,
  }) {
    DateTime now = DateTime.now();

    if (_daysDifferenceIgnoringTime(destinationDate, now) < 0 ) {
      return false;
    }

    if( _daysDifferenceIgnoringTime(sourceDate, destinationDate).abs() <= daysRange) {
      return true;
    }
    else {
      return false;
    }
  }

  static int _daysDifferenceIgnoringTime(DateTime date1, DateTime date2) {
    //positive + if date1 > date2
    //negative - if date1 < date2
    //ignoring hours, mins, secs, millisecs, microsecs.
    DateTime tempDate1 = date1.subtract(Duration(
        hours: date1.hour,
        minutes: date1.minute,
        seconds: date1.second,
        milliseconds: date1.millisecond,
        microseconds: date1.microsecond));

    DateTime tempDate2 = date2.subtract(Duration(
        hours: date2.hour,
        minutes: date2.minute,
        seconds: date2.second,
        milliseconds: date2.millisecond,
        microseconds: date2.microsecond));

    return tempDate1.difference(tempDate2).inDays;
  }
}
