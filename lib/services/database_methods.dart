import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as Path;
import 'package:shipment/screens/tabs_content/shipments.dart';
import 'dart:io';
import 'package:uuid/uuid.dart';

import 'package:shipment/services/auth_services/auth.dart';
import 'shipment_data.dart';
import 'trip_data.dart';

class DatabaseMethods {
  Future<bool> checkUserExists({userId}) async {
    bool exists;
    await Firestore.instance.document("users/$userId").get().then((doc) {
      exists = doc.exists;
    });
    print(exists);
    return exists;
  }

  Future<void> insertUserData({userId, fname, lname, email, phone}) async {
    await Firestore.instance.collection('users').document(userId).setData({
      'fname': fname,
      'lname': lname,
      'email': email,
      'phone': phone,
    });
  }

  Future<String> _uploadImageAndGetUrl(
      {File imageFile, String imageUniqueName}) async {
    StorageReference storageReference = FirebaseStorage.instance.ref().child(
        'profile_pictures/$imageUniqueName${Path.extension(imageFile.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(imageFile);
    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  Future<void> insertShipmentData({
    imageFile,
    description,
    dimension1,
    dimension2,
    dimension3,
    weight,
    fragile,
    countryFrom,
    stateFrom,
    countryTo,
    stateTo,
    date,
  }) async {
    String userId = await Auth().userId();
    String imageUniqueName = (userId + (Uuid().v1()));
    String imageUrl = await _uploadImageAndGetUrl(
        imageFile: imageFile, imageUniqueName: imageUniqueName);

    await Firestore.instance.collection('shipments').document().setData({
      'userId': userId,
      'description': description,
      'dimension1': dimension1,
      'dimension2': dimension2,
      'dimension3': dimension3,
      'weight': weight,
      'fragile': fragile,
      'countryFrom': countryFrom,
      'stateFrom': stateFrom,
      'countryTo': countryTo,
      'stateTo': stateTo,
      'date': date,
      'imageUrl': imageUrl,
    });
  }

  Future<void> insertTripData({
    description,
    countryFrom,
    stateFrom,
    countryTo,
    stateTo,
    means,
    date,
  }) async {
    String userId = await Auth().userId();
    await Firestore.instance.collection('trips').document().setData({
      'userId': userId,
      'description': description,
      'countryFrom': countryFrom,
      'stateFrom': stateFrom,
      'countryTo': countryTo,
      'stateTo': stateTo,
      'means': means,
      'date': date,
    });
  }

  Future<List<ShipmentData>> getMyShipmentsData() async {
    String userId = await Auth().userId();
    List<ShipmentData> shipmentsList = [];
    ShipmentData shipment;
    CollectionReference ref = Firestore.instance.collection('shipments');
    await ref
        .where("userId", isEqualTo: userId)
        .getDocuments()
        .then((query) async {
      for (int i = 0; i < query.documents.length; i++) {
        shipment = ShipmentData();
        shipment.userId = query.documents[i]['userId'];
        shipment.countryFrom = query.documents[i]['countryFrom'];
        shipment.countryTo = query.documents[i]['countryTo'];
        shipment.stateFrom = query.documents[i]['stateFrom'];
        shipment.stateTo = query.documents[i]['stateTo'];
        shipment.date =
            DateTime.parse((query.documents[i]['date']).toDate().toString());
        shipment.description = query.documents[i]['description'];
        shipment.dimension1 = query.documents[i]['dimension1'];
        shipment.dimension2 = query.documents[i]['dimension2'];
        shipment.dimension3 = query.documents[i]['dimension3'];
        shipment.weight = query.documents[i]['weight'];
        shipment.fragile = query.documents[i]['fragile'];
        shipment.imageUrl = query.documents[i]['imageUrl'];
        shipmentsList.add(shipment);
      }
    });
    //May have to sort the list..
    return shipmentsList;
  }

  Future<List<TripData>> getMyTripsData() async {
    String userId = await Auth().userId();
    List<TripData> tripsList = [];
    TripData trip;
    CollectionReference ref = Firestore.instance.collection('trips');
    await ref
        .where("userId", isEqualTo: userId)
        .getDocuments()
        .then((query) async {
      for (int i = 0; i < query.documents.length; i++) {
        trip = TripData();
        trip.userId = query.documents[i]['userId'];
        trip.countryFrom = query.documents[i]['countryFrom'];
        trip.countryTo = query.documents[i]['countryTo'];
        trip.stateFrom = query.documents[i]['stateFrom'];
        trip.stateTo = query.documents[i]['stateTo'];
        trip.date =
            DateTime.parse((query.documents[i]['date']).toDate().toString());
        trip.description = query.documents[i]['description'];
        trip.means = query.documents[i]['means'];
        tripsList.add(trip);
      }
    });
    //May have to sort the list..
    return tripsList;
  }

  // Future<List<TripData>> getMatchingTrips2({
  //   ShipmentData shipment,
  // }) async {
  //   List<TripData> tripsList = [];
  //   TripData trip;
  //   CollectionReference ref = Firestore.instance.collection('trips');
  //   //make date
  //   //make not same userId
  //   await ref
  //       .where("countryFrom", isEqualTo: shipment.countryFrom)
  //       .where("countryTo", isEqualTo: shipment.countryTo)
  //       .where("stateFrom", isEqualTo: shipment.stateFrom)
  //       .where("stateTo", isEqualTo: shipment.stateTo)
  //       .getDocuments()
  //       .then((query) async {
  //     for (int i = 0; i < query.documents.length; i++) {
  //       trip = TripData();
  //       trip.userId = query.documents[i]['userId'];
  //       trip.countryFrom = query.documents[i]['countryFrom'];
  //       trip.countryTo = query.documents[i]['countryTo'];
  //       trip.stateFrom = query.documents[i]['stateFrom'];
  //       trip.stateTo = query.documents[i]['stateFrom'];
  //       trip.date =
  //           DateTime.parse((query.documents[i]['date']).toDate().toString());
  //       trip.description = query.documents[i]['description'];
  //       trip.means = query.documents[i]['means'];
  //       tripsList.add(trip);
  //     }
  //   });
  //   return tripsList;
  // }

  Future<List<TripData>> getMatchingTripsListFromDocuments({
    List<DocumentSnapshot> docs,
  }) async {
    List<TripData> tripsList = [];
    TripData trip;
    for (int i = 0; i < docs.length; i++) {
      trip = TripData();
      trip.userId = docs[i]['userId'];
      trip.countryFrom = docs[i]['countryFrom'];
      trip.countryTo = docs[i]['countryTo'];
      trip.stateFrom = docs[i]['stateFrom'];
      trip.stateTo = docs[i]['stateTo'];
      trip.date = DateTime.parse((docs[i]['date']).toDate().toString());
      trip.description = docs[i]['description'];
      trip.means = docs[i]['means'];
      tripsList.add(trip);
    }
    return tripsList;
  }

  Future<List<ShipmentData>> getMatchingShipmentsListFromDocuments({
    List<DocumentSnapshot> docs,
  }) async {
    List<ShipmentData> shipmentsList = [];
    ShipmentData shipment;
    for (int i = 0; i < docs.length; i++) {
      shipment = ShipmentData();
      shipment.userId = docs[i]['userId'];
      shipment.countryFrom = docs[i]['countryFrom'];
      shipment.countryTo = docs[i]['countryTo'];
      shipment.stateFrom = docs[i]['stateFrom'];
      shipment.stateTo = docs[i]['stateTo'];
      shipment.date = DateTime.parse((docs[i]['date']).toDate().toString());
      shipment.description = docs[i]['description'];
      shipment.dimension1 = docs[i]['dimension1'];
      shipment.dimension2 = docs[i]['dimension2'];
      shipment.dimension3 = docs[i]['dimension3'];
      shipment.weight = docs[i]['weight'];
      shipment.fragile = docs[i]['fragile'];
      shipment.imageUrl = docs[i]['imageUrl'];
      shipmentsList.add(shipment);
    }
    return shipmentsList;
  }

  Future<String> getUserFirstName({userId}) async {
    if (userId == null) {
      return null;
    }
    String fname;
    await Firestore.instance
        .collection('users')
        .document(userId)
        .get()
        .then((doc) {
      fname = doc['fname'];
    });
    return fname;
  }

  Future<String> getUserLastName({userId}) async {
    if (userId == null) {
      return null;
    }
    String lname;
    await Firestore.instance
        .collection('users')
        .document(userId)
        .get()
        .then((doc) {
      lname = doc['lname'];
    });
    return lname;
  }

  Future<String> getUserFullName({userId}) async {
    if (userId == null) {
      return null;
    }
    return (await getUserFirstName(userId: userId) +
        ' ' +
        await getUserLastName(userId: userId));
  }
}
