import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  Future<String> userId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user != null ? user.uid : null;
  }

  Future<String> userEmail() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user.email;
  }
}
