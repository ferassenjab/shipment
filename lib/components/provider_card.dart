import 'package:flutter/material.dart';

class ProviderCard extends StatelessWidget {
  final String providerName;
  final String serviceDescription;
  final String imageUrl;
  final String distanceString;
  final Function onTap;

  ProviderCard({this.providerName,this.serviceDescription,this.imageUrl,this.distanceString,this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 8.0,
        child: Container(
          padding: const EdgeInsets.all(18.0),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                radius: 30.0,
                backgroundImage: NetworkImage(imageUrl),
                backgroundColor: Colors.transparent,
              ),
              SizedBox(width: 10.0,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      providerName,
                      style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                          fontSize: 17.0
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    Text(
                      serviceDescription,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    Row(
                      children: <Widget>[
                        Spacer(),
                        Text(
                          distanceString,
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Colors.red,
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )

            ],
          ),
        ),
      ),
    );

  }
}
