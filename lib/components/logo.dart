import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'hero',
      child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 60.0,
          child: ClipOval(
            child: Image.asset(
              'images/logo.png',
              fit: BoxFit.cover,//
              width: 120.0,
              height: 120.0,
            ),
          )),
    );
  }
}
