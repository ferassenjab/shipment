import 'package:flutter/material.dart';

class LabelButton extends StatelessWidget {
  final String labelText;
  final Function onPressed;

  LabelButton({@required this.labelText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        labelText,
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: onPressed,
    );
  }
}
