import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Function onPressed;
  final String buttonText;
  RoundedButton({@required this.buttonText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: onPressed,
      padding: EdgeInsets.all(12),
      color: Theme.of(context).primaryColor,
      child: Text(
          buttonText,
          style: TextStyle(color: Colors.white)),
    );
  }
}
