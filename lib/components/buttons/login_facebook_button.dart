import 'package:flutter/material.dart';

class LoginFacebookButton extends StatelessWidget {
  final Function onPressed;
  final String buttonText;
  LoginFacebookButton({@required this.buttonText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: onPressed,
      padding: EdgeInsets.all(12),
      color: Colors.blue[900],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
              buttonText,
              style: TextStyle(color: Colors.white)
              ),
          CircleAvatar(
            radius: 17.0,
            child: Image.asset('images/facebook.png'),
          )
        ],
      )
    );
  }
}
