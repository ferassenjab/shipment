import 'package:flutter/material.dart';

class LoginGoogleButton extends StatelessWidget {
  final Function onPressed;
  final String buttonText;
  LoginGoogleButton({@required this.buttonText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: onPressed,
      padding: EdgeInsets.all(12),
      color: Colors.red[700],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
              buttonText,
              style: TextStyle(color: Colors.white)
              ),
          CircleAvatar(
            radius: 17.0,
            child: Image.asset('images/google.png'),
          )
        ],
      )
    );
  }
}
