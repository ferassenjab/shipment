// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
// import 'dart:io';
// import 'package:fursatak/constants.dart';

// class ProfileImagePicker extends StatefulWidget {
//   final Function onTap;
//   ProfileImagePicker({@required this.onTap});
//   @override
//   _ProfileImagePickerState createState() => _ProfileImagePickerState();
// }

// class _ProfileImagePickerState extends State<ProfileImagePicker> {
//   File _galleryFile;

//   @override
//   Widget build(BuildContext context) {
//     return Hero(
//       tag: 'hero',
//       child: InkWell(
//         onTap: () async {
//           _galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
//           widget.onTap(_galleryFile);
//           setState(() {});
//         },
//         child: CircleAvatar(
//             backgroundColor: Colors.transparent,
//             radius: 60.0,
//             child: ClipOval(
//               child: (_galleryFile == null) ?
//               Image.asset(
//                 kProfileDefaultImageAsset,
//                 fit: BoxFit.cover,//
//                 width: 120.0,
//                 height: 120.0,
//                 ) :
//               Image.file(_galleryFile,
//                 fit: BoxFit.cover,//
//                 width: 120.0,
//                 height: 120.0,
//               ),
//             )),
//       ),
//     );
//   }
// }