import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class ImageUploaderWidget extends StatefulWidget {
  final defaultImageAssetPath;
  final Function onTap;
  ImageUploaderWidget({@required this.defaultImageAssetPath, @required this.onTap});

  @override
  _ImageUploaderWidgetState createState() => _ImageUploaderWidgetState();
}

class _ImageUploaderWidgetState extends State<ImageUploaderWidget> {
  File _galleryFile;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: (_galleryFile == null)
          ? Image.asset(widget.defaultImageAssetPath)
          : Image.file(_galleryFile),
      onTap: () async {
        FocusScope.of(context).requestFocus(new FocusNode());
        _galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
        widget.onTap(_galleryFile);
        setState(() {
        });
        print('clicked me?');
      },
    );
  }
}
