import 'package:flutter/material.dart';
import 'package:shipment/services/validator.dart';

class EmailTextFormField extends StatelessWidget {
  final TextEditingController emailController;
  final String hintText;
  EmailTextFormField({@required this.emailController, @required this.hintText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      controller: emailController,
      validator: Validator.validateEmail,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.email,
            color: Colors.grey,
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
}

class PasswordTextFormField extends StatelessWidget {
  final TextEditingController passwordController;
  final String hintText;
  PasswordTextFormField(
      {@required this.passwordController, @required this.hintText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,
      validator: Validator.validatePassword,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.lock,
            color: Colors.grey,
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
}

class NameTextFormField extends StatelessWidget {
  final TextEditingController nameController;
  final String hintText;
  final IconData icon;
  NameTextFormField({@required this.nameController, @required this.hintText, this.icon});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      textCapitalization: TextCapitalization.words,
      controller: nameController,
      validator: Validator.validateName,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            icon != null ? icon : Icons.person,
            color: Colors.grey,
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
}

class DescriptionTextFormField extends StatelessWidget {
  final TextEditingController descriptionController;
  final String hintText;
  final IconData icon;
  final borderRadius ;

  DescriptionTextFormField(
      {@required this.descriptionController, @required this.hintText, this.icon, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: descriptionController,
      validator: Validator.validateDescription,
      maxLines: 3,
      maxLength: 500,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            icon,
            color: Colors.grey,
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: '\n$hintText',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(borderRadius!=null ? borderRadius: 32.0)),
      ),
    );
  }
}

class PhoneNumberTextFormField extends StatelessWidget {
  final TextEditingController phoneController;
  final String hintText;

  PhoneNumberTextFormField(
      {@required this.phoneController, @required this.hintText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      controller: phoneController,
      keyboardType: TextInputType.phone,
      validator: Validator.validatePhone,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(left: 5.0),
          child: Icon(
            Icons.phone,
            color: Colors.grey,
          ), // icon is 48px widget.
        ), // icon is 48px widget.
        hintText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),      
    );
  }
}

class NumericTextFormField extends StatelessWidget {
  final TextEditingController numericController;
  final String hintText;
  final IconData icon;
  final borderRadius;

  NumericTextFormField(
      {@required this.numericController, @required this.hintText, this.icon, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      controller: numericController,
      keyboardType: TextInputType.number,
      validator: Validator.validateNumber,
      decoration: InputDecoration(
        
        // prefixIcon: Padding(
        //   padding: EdgeInsets.only(left: 0.0),
        //   child: Icon(
        //     icon,
        //     color: Colors.grey,
        //   ), // icon is 48px widget.
        // ), // icon is 48px widget.
        hintText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(borderRadius!=null ? borderRadius: 32.0))),
            
    );
  }
}


// class PhoneTextFormField extends StatefulWidget {
//   @override
//   _PhoneTextFormFieldState createState() => _PhoneTextFormFieldState();
// }

// class _PhoneTextFormFieldState extends State<PhoneTextFormField> {
//   String phoneNumber;
//   String phoneIsoCode;

//   void onPhoneNumberChange(
//       String number, String internationalizedPhoneNumber, String isoCode) {
//     setState(() {
//       phoneNumber = number;
//       phoneIsoCode = isoCode;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return InternationalPhoneInput(
//         onPhoneNumberChange: onPhoneNumberChange,
//         initialPhoneNumber: phoneNumber,
//         initialSelection: phoneIsoCode,
//         enabledCountries: ['+233', '+1']);
//   }

//   // onValidPhoneNumber(
//   //   String number, String internationalizedPhoneNumber, String isoCode) {
//   //   setState(() {
//   //     confirmedNumber = internationalizedPhoneNumber;
//   //   });
//   // }

//   // @override
//   //  Widget build(BuildContext context) {
//   //    return InternationalPhoneInputText(
//   //        onValidPhoneNumber: onValidPhoneNumber,
//   //       );
//   //  }
// }
