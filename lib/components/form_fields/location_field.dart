// import 'package:flutter/material.dart';
// import 'package:fursatak/services/validator.dart';

// class LocationField extends StatelessWidget {
//   final TextEditingController locationController;
//   final String hintText;
//   final Function onTap;

//   LocationField({@required this.locationController, @required this.hintText, @required this.onTap});

//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       autofocus: false,
//       controller: locationController,
//       validator: Validator.validateLocation,
//       readOnly: true,
//       onTap: onTap,
//       decoration: InputDecoration(
//         prefixIcon: Padding(
//           padding: EdgeInsets.only(left: 5.0),
//           child: Icon(
//             Icons.my_location,
//             color: Colors.grey,
//           ), // icon is 48px widget.
//         ), // icon is 48px widget.
//         hintText: hintText,
//         contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//         border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
//       ),
//     );
//   }
// }
