import 'package:flutter/material.dart';
import 'package:shipment/services/validator.dart';

class DropMenu extends StatefulWidget {
  final String hintText;
  final List<String> items;
  final Function onChanged;
  final IconData icon;
  final borderRadius;

  DropMenu({@required this.items, this.hintText, this.onChanged, this.icon, this.borderRadius});

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropMenu> {
  String selectedValue;
  @override
  Widget build(BuildContext context) {
    if (!widget.items.contains(selectedValue)) {
      selectedValue = null;
    }
    return DropdownButtonFormField<String>(
      value: selectedValue,
      isExpanded: true,
      isDense: true,
      validator: Validator.validateDropdownMenu,
      decoration: InputDecoration(
        // prefixIcon: Padding(
        //   padding: EdgeInsets.only(left: 5.0),
        //   child: Icon(
        //     widget.icon,
        //     color: Colors.grey,
        //   ), // icon is 48px widget.
        // ), // icon is 48px widget.
        //hintText: widget.hintText,
        contentPadding: EdgeInsets.fromLTRB(30.0, 5.0, 0.0, 5.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(widget.borderRadius!=null ? widget.borderRadius: 32.0)),
      ),
      hint: Text(widget.hintText),
      items: widget.items.map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
      onChanged: (value){
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          selectedValue = value;
          widget.onChanged(value);
        });
      },
    );
  }
}
