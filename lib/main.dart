import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shipment/screens/home_screen.dart';
import 'package:shipment/services/database_methods.dart';

import 'services/auth_services/auth.dart';

import 'screens/login_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  String userId = await Auth().userId();
  print(userId);
  String userFullName = (userId != null) ? await DatabaseMethods().getUserFullName(userId: userId) : '';
  bool userSignedin = (userId != null);
  runApp(MyApp(userSignedin: userSignedin, userId: userId, userFullname: userFullName,));
}

class MyApp extends StatefulWidget {
  final bool userSignedin;
  final String userId;
  final String userFullname;
  MyApp({this.userSignedin,this.userId,this.userFullname});
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
       home: widget.userSignedin ? HomeScreen(userId: widget.userId, userFullname: widget.userFullname) : LoginScreen(),
    );
  }
}

