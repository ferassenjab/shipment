import 'package:flutter/material.dart';
import 'dart:io';
import 'package:shipment/components/form_fields/text_fields.dart';
import 'package:shipment/components/image_uploader_widget.dart';
import 'package:shipment/components/buttons/rounded_button.dart';
import 'package:shipment/screens/home_screen.dart';

import 'package:shipment/services/countries_data.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/components/form_fields/drop_menu.dart';
import 'package:shipment/components/loading.dart';


class SendScreen extends StatefulWidget {
  @override
  _SendScreenState createState() => _SendScreenState();
}

class _SendScreenState extends State<SendScreen> {
  final borderRadius = 15.0;

  bool _autoValidate = false;
  bool _loadingVisible = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _demintion1Controller = TextEditingController();
  TextEditingController _demintion2Controller = TextEditingController();
  TextEditingController _demintion3Controller = TextEditingController();

  TextEditingController _weightController = TextEditingController();

  String fragile, countryFrom, stateFrom, countryTo, stateTo;
  List<String> _countryStatesFrom = [];
  List<String> _countryStatesTo = [];

  File galleryFile;
  String imageAnnouncement = '';

  DateTime selectedDate;
  String selectDateAnnouncement = '';

  Future<DateTime> _selectDate(BuildContext context) async {
    DateTime now = DateTime.now();
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: now,
        lastDate: DateTime(2100));
    return picked;
  }

  @override
  Widget build(BuildContext context) {

    final description = DescriptionTextFormField(
      descriptionController: _descriptionController,
      hintText: 'Description of shipment',
      borderRadius: borderRadius,
    );

    final imagePicker = ImageUploaderWidget(
      defaultImageAssetPath: 'images/cloud.png',
      onTap: (value) {
        galleryFile = value;
        if (galleryFile != null) {
          setState(() {
            imageAnnouncement = '';
          });
        }
      },
    );

    final dimension1 = NumericTextFormField(
      hintText: '0',
      numericController: _demintion1Controller,
      borderRadius: borderRadius,
    );

    final dimension2 = NumericTextFormField(
      hintText: '0',
      numericController: _demintion2Controller,
      borderRadius: borderRadius,
    );

    final dimension3 = NumericTextFormField(
      hintText: '0',
      numericController: _demintion3Controller,
      borderRadius: borderRadius,
    );

    //dem2 , dem3

    final weight = NumericTextFormField(
      hintText: '0',
      numericController: _weightController,
      borderRadius: borderRadius,
    );

    final fragileMenu = DropMenu(
      hintText: 'Yes/No',
      items: ['Yes', 'No'],
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          fragile = value;
        });
      },
    );

    final countriesFromMenu = FutureBuilder(
      future: CountriesData().getCountries(),
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        if (snapshot.hasData) {
          return DropMenu(
            items: snapshot.data,
            hintText: 'Country',
            borderRadius: borderRadius,
            onChanged: (value) async {
              _countryStatesFrom =
                  await CountriesData().getStates(countryName: value);
              setState(() {
                countryFrom = value;
                stateFrom = null;
              });
            },
          );
        } else {
          return DropMenu(
            hintText: 'Loading Countries...',
            items: [],
            borderRadius: borderRadius,
          );
        }
      },
    );

    final countriesToMenu = FutureBuilder(
      future: CountriesData().getCountries(),
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        if (snapshot.hasData) {
          return DropMenu(
            items: snapshot.data,
            hintText: 'Country',
            borderRadius: borderRadius,
            onChanged: (value) async {
              _countryStatesTo =
                  await CountriesData().getStates(countryName: value);
              setState(() {
                countryTo = value;
                stateTo = null;
              });
            },
          );
        } else {
          return DropMenu(
            hintText: 'Loading Countries...',
            items: [],
            borderRadius: borderRadius,
          );
        }
      },
    );

    final statesFromMenu = DropMenu(
      hintText: 'State',
      items: _countryStatesFrom,
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          stateFrom = value;
        });
      },
    );

    final statesToMenu = DropMenu(
      hintText: 'State',
      items: _countryStatesTo,
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          stateTo = value;
        });
      },
    );

    final dateButton = RaisedButton(
      child: Text('Select Date'),
      onPressed: () async {
        FocusScope.of(context).requestFocus(new FocusNode());
        DateTime newDateTimeValue = await _selectDate(context);
        if (newDateTimeValue != null) {
          setState(() {
            selectedDate = newDateTimeValue;
          });
        }
      },
    );

    Future<void> _changeLoadingVisible() async {
      setState(() {
        _loadingVisible = !_loadingVisible;
      });
    }

    final submitButton = RoundedButton(
        buttonText: 'Submit',
        onPressed: () async {
          print('you clicked me');
          if (_formKey.currentState.validate() &&
              galleryFile != null &&
              selectedDate != null) {
            _changeLoadingVisible();
            try {
              await DatabaseMethods().insertShipmentData(
                description: _descriptionController.text,
                dimension1: _demintion1Controller.text,
                dimension2: _demintion2Controller.text,
                dimension3: _demintion3Controller.text,
                weight: _weightController.text,
                fragile: fragile,
                countryFrom: countryFrom,
                countryTo: countryTo,
                stateFrom: stateFrom,
                stateTo: stateTo,
                date: selectedDate,
                imageFile: galleryFile,
              );
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return HomeScreen();
              }));
            } catch (e) {
              print(e);
            }
            _changeLoadingVisible();
          } else {
            if (galleryFile == null) {
              imageAnnouncement = 'Pick An Image!';
            }
            if (selectedDate == null) {
              selectDateAnnouncement = 'Please Select Date!';
            }
            setState(() {
              _autoValidate = true;
            });
          }
        });
    return LoadingScreen(
      inAsyncCall: _loadingVisible,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Send Shipment'),
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Fill Information About Shipment',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    description,
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      title: Text(
                        'Dimensions by centimeter\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Flexible(
                            child: dimension1,
                          ),
                          Text(
                            ' X ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Flexible(
                            child: dimension2,
                          ),
                          Text(
                            ' X ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Flexible(
                            child: dimension3,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Text(
                                'Upload shipment image\n',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              imagePicker,
                              Text(
                                imageAnnouncement,
                                style: TextStyle(color: Colors.red),
                              ),
                            ],
                          )),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    'Weight by Kg\n',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  subtitle: weight,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                ListTile(
                                  title: Text(
                                    'Is it fragile?\n',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  subtitle: fragileMenu,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'From (Country / State)\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Expanded(
                            child: countriesFromMenu,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Expanded(child: statesFromMenu),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    ListTile(
                      title: Text(
                        'To (Country / State)\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Expanded(
                            child: countriesToMenu,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Expanded(child: statesToMenu),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      leading: Icon(Icons.date_range),
                      title: Text(
                        'Required date of transmission\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        selectedDate != null
                            ? '${selectedDate.day}-${selectedDate.month}-${selectedDate.year}'
                            : selectDateAnnouncement,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      trailing: dateButton,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    submitButton,
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
