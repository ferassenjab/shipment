import 'package:flutter/material.dart';
import 'package:shipment/screens/chat_screen.dart';
import 'package:shipment/services/auth_services/auth.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/services/shipment_data.dart';

class MatchingShipmentsScreen extends StatefulWidget {
  final List<ShipmentData> shipmentsList;
  MatchingShipmentsScreen({this.shipmentsList});
  @override
  _MatchingShipmentsScreenState createState() =>
      _MatchingShipmentsScreenState();
}

class _MatchingShipmentsScreenState extends State<MatchingShipmentsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Matching Shipments'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
        child: SingleChildScrollView(
          child: Column(
            children: List.generate(widget.shipmentsList.length, (index) {
              var shipment = widget.shipmentsList[index];
              return FutureBuilder(
                  future: DatabaseMethods()
                      .getUserFullName(userId: shipment.userId),
                  builder: (context, AsyncSnapshot<String> snapshot) {
                    if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}');
                    } else if (!snapshot.hasData) {
                      return Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 60.0,
                            ),
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: SizedBox(
                                  child: CircularProgressIndicator(),
                                  height: 60.0,
                                  width: 60.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    } else {
                      return _matchingShipmentCard(
                        context: context,
                        fullName: snapshot.data,
                        id: shipment.userId,
                        countryFrom: shipment.countryFrom,
                        countryTo: shipment.countryTo,
                        stateFrom: shipment.stateFrom,
                        stateTo: shipment.stateTo,
                        date: shipment.date,
                        description: shipment.description,
                        dimension1: shipment.dimension1,
                        dimension2: shipment.dimension2,
                        dimension3: shipment.dimension3,
                        fragile: shipment.fragile,
                        weight: shipment.weight,
                        imageUrl: shipment.imageUrl,
                      );
                    }
                  });
            }),
          ),
        ),
      ),
    );
  }
}

Widget _matchingShipmentCard({
  id,
  context,
  fullName,
  imageUrl,
  countryFrom,
  stateFrom,
  countryTo,
  stateTo,
  DateTime date,
  description,
  dimension1,
  dimension2,
  dimension3,
  weight,
  fragile,
}) {
  return Card(
    elevation: 8.0,
    child: Container(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: <Widget>[
          Center(
            child: RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 15.0,
                ),
                children: [
                  TextSpan(
                    text: fullName,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                  TextSpan(
                      text: ' has a shipment!',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black))
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Container(
                child: Image.network(
                  imageUrl,
                  height: 80.0,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'From: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$countryFrom, $stateFrom',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'To: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$countryTo, $stateTo',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'On: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '${date.day}-${date.month}-${date.year}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Dimensions: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text:
                                  '$dimension1 x $dimension2 x $dimension3 cm',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Weight: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$weight kg',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Fragile: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$fragile',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            child: Text(
              description,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Spacer(),
              FlatButton(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.mail_outline,
                      color: Colors.green,
                    ),
                    Text(
                      '  Click to contact',
                      style: TextStyle(
                          color: Colors.green, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                onPressed: () async {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                            child: CircularProgressIndicator(),
                            height: 60.0,
                            width: 60.0,
                          ),
                        ),
                      );
                    },
                  );
                  String myId = await Auth().userId();
                  String myFullName =
                      await DatabaseMethods().getUserFullName(userId: myId);
                  String receiverId = id;
                  String receiverFullName = fullName;
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) {
                    return ChatScreen(
                      senderId: myId,
                      senderName: myFullName,
                      receiverId: receiverId,
                      receiverName: receiverFullName,
                    );
                  }));
                },
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
