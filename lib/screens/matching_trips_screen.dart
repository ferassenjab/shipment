import 'package:flutter/material.dart';
import 'package:shipment/services/auth_services/auth.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/services/trip_data.dart';

import 'chat_screen.dart';

class MatchingTripsScreen extends StatefulWidget {
  final List<TripData> tripsList;

  MatchingTripsScreen({this.tripsList});

  @override
  _MatchingTripsScreenState createState() => _MatchingTripsScreenState();
}

class _MatchingTripsScreenState extends State<MatchingTripsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Matching Trips'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
        child: SingleChildScrollView(
          //
          child: Column(
            children: List.generate(widget.tripsList.length, (index) {
              var trip = widget.tripsList[index];
              return FutureBuilder(
                future: DatabaseMethods().getUserFullName(userId: trip.userId),
                builder: (context, AsyncSnapshot<String> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else if (!snapshot.hasData) {
                    return Center(
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 60.0,
                          ),
                          Container(
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: SizedBox(
                                child: CircularProgressIndicator(),
                                height: 60.0,
                                width: 60.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return _matchingTripCard(
                      context: context,
                      id: trip.userId,
                      fullName: snapshot.data,
                      countryFrom: trip.countryFrom,
                      countryTo: trip.countryTo,
                      stateFrom: trip.stateFrom,
                      stateTo: trip.stateTo,
                      date: trip.date,
                      means: trip.means,
                      description: trip.description,
                      //matches: 5,
                    );
                  }
                },
              );
            }),
          ),
        ),
      ),
    );
  }
}

Widget _matchingTripCard({
  context,
  id,
  fullName,
  countryFrom,
  stateFrom,
  countryTo,
  stateTo,
  means,
  date,
  description,
  //int matches,
}) {
  return Card(
    elevation: 8.0,
    child: Container(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            //mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                    children: [
                      TextSpan(
                        text: fullName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        ),
                      ),
                      TextSpan(
                          text: ' is going a trip!',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.black))
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'From: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '$countryFrom, $stateFrom',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'To: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '$countryTo, $stateTo',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'On: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '${date.day}-${date.month}-${date.year}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'Going by: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: means,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            child: Text(
              description,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Spacer(),
              FlatButton(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.mail_outline,
                      color: Colors.green,
                    ),
                    Text(
                      '  Click to contact',
                      style: TextStyle(
                          color: Colors.green, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                onPressed: () async {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                            child: CircularProgressIndicator(),
                            height: 60.0,
                            width: 60.0,
                          ),
                        ),
                      );
                    },
                  );
                  String myId = await Auth().userId();
                  String myFullName =
                      await DatabaseMethods().getUserFullName(userId: myId);
                  String receiverId = id;
                  String receiverFullName = fullName;
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) {
                    return ChatScreen(
                      senderId: myId,
                      senderName: myFullName,
                      receiverId: receiverId,
                      receiverName: receiverFullName,
                    );
                  }));
                },
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
