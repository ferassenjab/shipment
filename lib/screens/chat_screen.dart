import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shipment/components/chat_bubble.dart';
import 'package:ntp/ntp.dart';


final _firestore = Firestore.instance;


class ChatScreen extends StatefulWidget {
  final String senderId, receiverId, senderName, receiverName;

  ChatScreen({@required this.senderId,
              @required this.receiverId,
              @required this.senderName,
              @required this.receiverName});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();

  String messageText;

  String oneToOneChatId() {
    ///Generate chat id by combining senderId and receiverId
    if (widget.senderId.compareTo(widget.receiverId) == -1) // senderId < receiverId
    {
      return widget.senderId + widget.receiverId;
    }
    else {
      return widget.receiverId + widget.senderId;
    }
  }

  void addLastMessageToReceiverInbox ({messageText, time, bool sentByMe}) async {
    ///when the user send message it should be added to
    ///inbox of receiver if it was first chat between both
    ///or should update receiver's inbox to set the new message
    ///as last message if it wasn't the first chat between both
    await _firestore.collection('chat')
        .document('inbox')
        .collection(widget.receiverId)
        .document(widget.senderId).setData(
      {
        'contact_name': widget.senderName,
        'last_message': messageText,
        'time' : time,
        'sent_by_me' : sentByMe,
        'seen' : false,
      }
    );
  }

  void addLastMessageToSenderInbox({messageText, time, bool sentByMe}) async {
  ///same as previous method but to add to sender inbox
  await _firestore.collection('chat')
      .document('inbox')
      .collection(widget.senderId)
      .document(widget.receiverId).setData(
    {
      'contact_name': widget.receiverName,
      'last_message': messageText,
      'time' : time,
      'sent_by_me' : sentByMe,
      'seen' : true,
    }
  );
}

  @override
  Widget build(BuildContext context) {
    final CollectionReference collectionReference = _firestore.collection('chat')
        .document('messages')
        .collection(oneToOneChatId());
    return Scaffold(
      appBar: AppBar(title: Text('Chat with ${widget.receiverName}')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          MessagesStream(collectionReference: collectionReference,
              currentUserId: widget.senderId,
          ),

          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide( width: 2.0),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                      hintText: 'Type your message here...',
                      border: InputBorder.none,
                    ),
                    controller: messageTextController,
                    onChanged: (value) {
                      messageText = value;
                    },
                  ),
                ),
                FlatButton(
                  onPressed: () async{
                    messageTextController.clear();
                    int currentTime = (await NTP.now()).toUtc().millisecondsSinceEpoch;
                    await collectionReference.add({
                      'sender_id': widget.senderId,
                      'receiver_id': widget.receiverId,
                      'sender_name': widget.senderName,
                      'receiver_name' : widget.receiverName,
                      'message': messageText,
                      'time' : currentTime,
                    }).then((_){
                      addLastMessageToReceiverInbox(
                        messageText: messageText,
                        time: currentTime,
                        sentByMe: false,
                      );
                    }).then((_){
                      addLastMessageToSenderInbox(
                        messageText: messageText,
                        time: currentTime,
                        sentByMe: true,
                      );
                    });

                  },

                  child: Text(
                    'Send',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class MessagesStream extends StatelessWidget {
  final CollectionReference collectionReference;
  final currentUserId;
  MessagesStream({@required this.collectionReference,
    @required this.currentUserId,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: collectionReference.orderBy('time',descending: true).snapshots(),
      builder: (context, snapshot){
        if(!snapshot.hasData){
          return Center(
            child: SizedBox(
              child: CircularProgressIndicator(
                backgroundColor: Colors.blue,
              ),
              width: 60,
              height: 60,
            ),
          );
        }
        else if(snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        else {
          final messages = snapshot.data.documents;
          List<ChatBubble> chatBubbles = [];
          for(var message in messages) {
            final messageText = message.data['message'];
            final senderId = message.data['sender_id'];
            chatBubbles.add(ChatBubble(
              text: messageText,
              isMe: currentUserId == senderId,
            ));
          }

          if (chatBubbles.length > 0 && !chatBubbles[0].isMe) {
            _firestore.collection('chat')
                .document('inbox')
                .collection(currentUserId)
                .document(messages[0].data['sender_id']).updateData({
              'seen' : true,
            });
          }

          return Expanded(
            child: ListView(
              children: chatBubbles,
              reverse: true,
            ),
          );
        }
      },
    );
  }
}