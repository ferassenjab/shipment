import 'package:flutter/material.dart';
import 'tabs_content/home_content.dart';
import 'tabs_content/shipments.dart';
import 'tabs_content/trips.dart';
import 'tabs_content/inbox.dart';

class HomeScreen extends StatefulWidget {
  final String userId;
  final String userFullname;

  HomeScreen({this.userId, this.userFullname});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<String> _appbarTitles = [
    'Home Page',
    'My Shipments',
    'My Trips',
    'Inbox'
  ];
  String _myTitle;
  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    _myTitle = _appbarTitles[0];
    _tabController.addListener(_handleSelectedTitle);
    super.initState();
    
  }

  void _handleSelectedTitle() {
    setState(() {
       _myTitle= _appbarTitles[_tabController.index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_myTitle),
        bottom: TabBar(
          controller: _tabController,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.amber,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home),
            ),
            Tab(
              icon: Icon(Icons.work),
            ),
            Tab(
              icon: Icon(Icons.airplanemode_active),
            ),
            Tab(
              icon: Icon(Icons.mail),
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          HomeContent(),
          Shipment(),
          Trips(),
          Inbox(
              currentUserId: widget.userId,
              currentUserName: widget.userFullname),
        ],
      ),
    );
  }
}
