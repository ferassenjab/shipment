import 'package:flutter/material.dart';
import 'package:shipment/components/form_fields/text_fields.dart';
import 'package:shipment/components/buttons/rounded_button.dart';
import 'package:shipment/screens/home_screen.dart';
import 'package:shipment/services/countries_data.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/components/form_fields/drop_menu.dart';
import 'package:shipment/components/loading.dart';

class GoSomewhereScreen extends StatefulWidget {
  @override
  _GoSomewhereScreenState createState() => _GoSomewhereScreenState();
}

class _GoSomewhereScreenState extends State<GoSomewhereScreen> {
  final borderRadius = 15.0;
  bool _autoValidate = false;
  bool _loadingVisible = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _descriptionController = TextEditingController();
  String meansOfTrip, flixibility, countryFrom, stateFrom, countryTo, stateTo;
  final List<String> _meansList = [
    'Plane',
    'Ship',
    'Car',
    'Bus',
    'Motorcycle',
    'Bicycle',
    'Scooter',
    'Other'
  ];
  List<String> _countryStatesFrom = [];
  List<String> _countryStatesTo = [];

  DateTime selectedDate;
  String selectDateAnnouncement = '';

  Future<DateTime> _selectDate(BuildContext context) async {
    DateTime now = DateTime.now();
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: now,
        lastDate: DateTime(2100));
    return picked;
  }

  @override
  Widget build(BuildContext context) {
    final description = DescriptionTextFormField(
      descriptionController: _descriptionController,
      hintText: 'Description of Trip',
      borderRadius: borderRadius,
    );

    final countriesFromMenu = FutureBuilder(
      future: CountriesData().getCountries(),
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        if (snapshot.hasData) {
          return DropMenu(
            items: snapshot.data,
            hintText: 'Country',
            borderRadius: borderRadius,
            onChanged: (value) async {
              _countryStatesFrom =
                  await CountriesData().getStates(countryName: value);
              setState(() {
                countryFrom = value;
                stateFrom = null;
              });
            },
          );
        } else {
          return DropMenu(
            hintText: 'Loading Countries...',
            items: [],
            borderRadius: borderRadius,
          );
        }
      },
    );

    final countriesToMenu = FutureBuilder(
      future: CountriesData().getCountries(),
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        if (snapshot.hasData) {
          return DropMenu(
            items: snapshot.data,
            hintText: 'Country',
            borderRadius: borderRadius,
            onChanged: (value) async {
              _countryStatesTo =
                  await CountriesData().getStates(countryName: value);
              setState(() {
                countryTo = value;
                stateTo = null;
              });
            },
          );
        } else {
          return DropMenu(
            hintText: 'Loading Countries...',
            items: [],
            borderRadius: borderRadius,
          );
        }
      },
    );

    final statesFromMenu = DropMenu(
      hintText: 'State',
      items: _countryStatesFrom,
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          stateFrom = value;
        });
      },
    );

    final statesToMenu = DropMenu(
      hintText: 'State',
      items: _countryStatesTo,
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          stateTo = value;
        });
      },
    );

    final meansMenu = DropMenu(
      hintText: 'Means',
      items: _meansList,
      borderRadius: borderRadius,
      onChanged: (value) {
        setState(() {
          meansOfTrip = value;
        });
      },
    );

    final dateButton = RaisedButton(
      child: Text('Select Date'),
      onPressed: () async {
        FocusScope.of(context).requestFocus(new FocusNode());
        DateTime newDateTimeValue = await _selectDate(context);
        if (newDateTimeValue != null) {
          setState(() {
            selectedDate = newDateTimeValue;
          });
        }
      },
    );

    Future<void> _changeLoadingVisible() async {
      setState(() {
        _loadingVisible = !_loadingVisible;
      });
    }

    final submitButton = RoundedButton(
        buttonText: 'Submit',
        onPressed: () async {
          if (_formKey.currentState.validate() && selectedDate != null) {
            _changeLoadingVisible();
            try {
              await DatabaseMethods().insertTripData(
                description: _descriptionController.text,
                countryFrom: countryFrom,
                stateFrom: stateFrom,
                countryTo: countryTo,
                stateTo: stateTo,
                means: meansOfTrip,
                date: selectedDate,
              );

              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return HomeScreen();
              }));

            } catch (e) {
              print(e);
            }
            _changeLoadingVisible();
          } else {
            if (selectedDate == null) {
              selectDateAnnouncement = 'Please Select Date!';
            }
            setState(() {
              _autoValidate = true;
            });
          }
        });
    
    return LoadingScreen(
      inAsyncCall: _loadingVisible,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Go Somewhere'),
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Fill Information About Trip',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    description,
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      title: Text(
                        'From (Country / State)\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Expanded(
                            child: countriesFromMenu,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Expanded(child: statesFromMenu),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    ListTile(
                      title: Text(
                        'To (Country / State)\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Expanded(
                            child: countriesToMenu,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Expanded(child: statesToMenu),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      title: Text(
                        'By What Means?\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Expanded(
                            child: meansMenu,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      leading: Icon(Icons.date_range),
                      title: Text(
                        'Expected date of arrival\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        selectedDate != null
                            ? '${selectedDate.day}-${selectedDate.month}-${selectedDate.year}'
                            : selectDateAnnouncement,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      trailing: dateButton,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    submitButton,
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
