import 'package:flutter/material.dart';
import 'package:shipment/screens/home_screen.dart';
import 'package:shipment/services/auth_services/auth.dart';
import 'package:shipment/services/database_methods.dart';

import 'package:shipment/components/form_fields/text_fields.dart';
import 'package:shipment/components/buttons/rounded_button.dart';

class GoogleFacebookSignup extends StatefulWidget {
  @override
  _GoogleFacebookSignupState createState() => _GoogleFacebookSignupState();
}

class _GoogleFacebookSignupState extends State<GoogleFacebookSignup> {
  bool _autoValidate = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final firstName = NameTextFormField(
      hintText: 'First Name',
      nameController: _firstNameController,
    );

    final lastName = NameTextFormField(
      hintText: 'Last Name',
      nameController: _lastNameController,
      //icon: Icons.person_outline,
    );

    final phoneNumber = PhoneNumberTextFormField(
      hintText: 'Phone Number',
      phoneController: _phoneController,
    );

    final continueButton = RoundedButton(
      buttonText: 'Continue',
      onPressed: () async {
        if (_formKey.currentState.validate()) {
          DatabaseMethods()
              .insertUserData(
            userId: await Auth().userId(),
            email: await Auth().userEmail(),
            fname: _firstNameController.text,
            lname: _lastNameController.text,
            phone: _phoneController.text,
          )
              .whenComplete(() {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen()));
          });
        }
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  firstName,
                  SizedBox(height: 24.0),
                  lastName,
                  SizedBox(height: 24.0),
                  phoneNumber,
                  SizedBox(height: 24.0),
                  continueButton,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
