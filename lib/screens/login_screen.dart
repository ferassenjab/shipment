import 'package:flutter/material.dart';
import 'package:shipment/services/auth_services/google_auth.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/services/auth_services/auth.dart';

import 'home_screen.dart';
import 'signup_screens/google_facebook_signup.dart';

import 'package:shipment/components/form_fields/text_fields.dart';
import 'package:shipment/components/logo.dart';
import 'package:shipment/components/buttons/rounded_button.dart';
import 'package:shipment/components/label_button.dart';
import 'package:shipment/components/loading.dart';
import 'package:shipment/components/buttons/login_google_button.dart';
import 'package:shipment/components/buttons/login_facebook_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _autoValidate = false;
  bool _loadingVisible = false;

  Future<void> _changeLoadingVisible() async {
    setState(() {
      _loadingVisible = !_loadingVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    final logo = Logo();

    final email = EmailTextFormField(
      emailController: _emailController,
      hintText: 'Email',
    );

    final password = PasswordTextFormField(
      passwordController: _passwordController,
      hintText: 'Password',
    );

    final loginButton = RoundedButton(
      buttonText: 'Login',
      onPressed: () {
        // _emailLogin(email: _emailController.text, password: _passwordController.text);
      },
    );

    final loginGoogleButton = LoginGoogleButton(
      buttonText: 'Login with Gmail',
      onPressed: () async {
        _changeLoadingVisible();
        try {
          await GoogleAuth().signInWithGoogle().then((userId) async {
            print('userId: $userId');
            if (await DatabaseMethods().checkUserExists(userId: userId)) {
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
                return HomeScreen();
              }));
            } else {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return GoogleFacebookSignup();
              }));
            }
          });
        } catch (e) {
          print(e);
        }

        _changeLoadingVisible();
      },
    );

    final loginFacebookButton = LoginFacebookButton(
      buttonText: 'Login with Facebook',
      onPressed: () {},
    );

    final forgotLabel = LabelButton(
      labelText: 'Forgot Password?',
      onPressed: () {
        //Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPasswordScreen()));
      },
    );

    final signUpLabel = LabelButton(
      labelText: 'Register Now',
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: LoadingScreen(
        inAsyncCall: _loadingVisible,
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    logo,
                    SizedBox(height: 48.0),
                    email,
                    SizedBox(height: 24.0),
                    password,
                    SizedBox(height: 12.0),
                    loginButton,
                    forgotLabel,
                    signUpLabel,
                    SizedBox(
                      height: 12.0,
                    ),
                    loginGoogleButton,
                    SizedBox(
                      height: 12.0,
                    ),
                    loginFacebookButton,
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
