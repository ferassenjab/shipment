import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/services/shipment_data.dart';
import 'package:shipment/services/trip_data.dart';

import '../matching_shipments_screen.dart';
import 'package:shipment/services/date_filter.dart';
import 'package:shipment/constants.dart';

class Trips extends StatefulWidget {
  @override
  _TripsState createState() => _TripsState();
}

class _TripsState extends State<Trips> {
  Future<List<TripData>> _myTrips = DatabaseMethods().getMyTripsData();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
      child: SingleChildScrollView(
        child: FutureBuilder(
          future: _myTrips,
          builder: (context, AsyncSnapshot<List<TripData>> snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: List.generate(snapshot.data.length, (index) {
                  var trip = snapshot.data[index];
                  return _myTripCard(
                    countryFrom: trip.countryFrom,
                    countryTo: trip.countryTo,
                    stateFrom: trip.stateFrom,
                    stateTo: trip.stateTo,
                    date: trip.date,
                    means: trip.means,
                    description: trip.description,
                    matches: MatchingShipmentsStream(
                      trip: trip,
                    ),
                  );
                }),
              );
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              return Column(
                children: <Widget>[
                  SizedBox(
                    height: 60.0,
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 60.0,
                        width: 60.0,
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}

Widget _myTripCard(
    {countryFrom,
    stateFrom,
    countryTo,
    stateTo,
    means,
    date,
    description,
    MatchingShipmentsStream matches}) {
  return Card(
    elevation: 8.0,
    child: Container(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            //mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'From: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '$countryFrom, $stateFrom',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'To: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '$countryTo, $stateTo',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'On: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: '${date.day}-${date.month}-${date.year}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'Going by: ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        )),
                    TextSpan(
                        text: means,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black))
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            child: Text(
              description,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Spacer(),
              Text(
                'Matches: ',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              ),
              matches,
            ],
          ),
        ],
      ),
    ),
  );
}

class MatchingShipmentsStream extends StatelessWidget {
  final TripData trip;
  MatchingShipmentsStream({this.trip});
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('shipments')
          .where("countryFrom", isEqualTo: trip.countryFrom)
          .where("countryTo", isEqualTo: trip.countryTo)
          .where("stateFrom", isEqualTo: trip.stateFrom)
          .where("stateTo", isEqualTo: trip.stateTo)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var filterdDocs = snapshot.data.documents
              .where(
                (doc) =>
                    doc['userId'] != trip.userId &&
                    DateFilter.compareDatesWithRangeAndNow(
                      sourceDate: trip.date,
                      destinationDate:
                          DateTime.parse((doc['date']).toDate().toString()),
                      daysRange: kDatesDaysRange,
                    ),
              )
              .toList();
          if (filterdDocs.length == 0) {
            return Text(
              '0',
              style: TextStyle(fontWeight: FontWeight.bold),
            );
          } else {
            return Row(
              children: <Widget>[
                Text(
                  filterdDocs.length.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                FlatButton(
                  child: Text(
                    'Click to check',
                    style: TextStyle(color: Colors.green),
                  ),
                  onPressed: () async {
                    List<ShipmentData> shipmentsList = await DatabaseMethods()
                        .getMatchingShipmentsListFromDocuments(
                            docs: filterdDocs);
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return MatchingShipmentsScreen(
                        shipmentsList: shipmentsList,
                      );
                    }));
                  },
                ),
              ],
            );
          }
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          return Text(
            'Loading matches..',
            style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
          );
        }
      },
    );
  }
}
