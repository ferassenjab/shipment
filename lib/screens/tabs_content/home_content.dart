import 'package:flutter/material.dart';
import 'package:shipment/services/auth_services/google_auth.dart';
import 'package:shipment/screens/login_screen.dart';
import 'package:shipment/screens/send_screen.dart';
import 'package:shipment/screens/go_somewhere_screen.dart';

class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('home screen'),
        RaisedButton(
          child: Text('LogOut'),
          onPressed: () {
            GoogleAuth().signOutGoogle().whenComplete(() {
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                return LoginScreen();
              }));
            });
          },
        ),
        RaisedButton(
          child: Text('Send shipment'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return SendScreen();
            }));
          },
        ),
        RaisedButton(
          child: Text('Go Somewhere'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return GoSomewhereScreen();
            }));
          },
        )
      ],
    ));
  }
}
