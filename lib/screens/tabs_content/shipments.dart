import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:shipment/services/database_methods.dart';
import 'package:shipment/services/shipment_data.dart';
import 'package:shipment/services/trip_data.dart';

import 'package:shipment/screens/matching_trips_screen.dart';
import 'package:shipment/services/date_filter.dart';
import 'package:shipment/constants.dart';

class Shipment extends StatefulWidget {
  @override
  _ShipmentState createState() => _ShipmentState();
}

class _ShipmentState extends State<Shipment> {
  Future<List<ShipmentData>> _myShipments =
      DatabaseMethods().getMyShipmentsData();

// StreamBuilder<List<TripData>> matchingTripsStream ( StreamController<ShipmentData> shipment){
//   return StreamBuilder(
//     stream: shipment,
//   );
// }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
      child: SingleChildScrollView(
        child: FutureBuilder(
          future: _myShipments,
          builder: (context, AsyncSnapshot<List<ShipmentData>> snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: List.generate(snapshot.data.length, (index) {
                  var shipment = snapshot.data[index];
                  return _myShipmentCard(
                    countryFrom: shipment.countryFrom,
                    countryTo: shipment.countryTo,
                    stateFrom: shipment.stateFrom,
                    stateTo: shipment.stateTo,
                    date: shipment.date,
                    dimension1: shipment.dimension1,
                    dimension2: shipment.dimension2,
                    dimension3: shipment.dimension3,
                    weight: shipment.weight,
                    fragile: shipment.fragile,
                    description: shipment.description,
                    imageUrl: shipment.imageUrl,
                    matches: MatchingTripsStream(shipment: shipment),
                  );
                }),
              );
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              return Column(
                children: <Widget>[
                  SizedBox(
                    height: 60.0,
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 60.0,
                        width: 60.0,
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}

Widget _myShipmentCard(
    {imageUrl,
    countryFrom,
    stateFrom,
    countryTo,
    stateTo,
    DateTime date,
    description,
    dimension1,
    dimension2,
    dimension3,
    weight,
    fragile,
    MatchingTripsStream matches}) {
  return Card(
    elevation: 8.0,
    child: Container(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Image.network(
                  imageUrl,
                  height: 80.0,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'From: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$countryFrom, $stateFrom',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'To: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$countryTo, $stateTo',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'On: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '${date.day}-${date.month}-${date.year}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Dimensions: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text:
                                  '$dimension1 x $dimension2 x $dimension3 cm',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Weight: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$weight kg',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Fragile: ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              )),
                          TextSpan(
                              text: '$fragile',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            child: Text(
              description,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Spacer(),
              Text(
                'Matches: ',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              ),
              matches,
            ],
          ),
        ],
      ),
    ),
  );
}

class MatchingTripsStream extends StatelessWidget {
  final ShipmentData shipment;
  MatchingTripsStream({this.shipment});
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('trips')
          .where("countryFrom", isEqualTo: shipment.countryFrom)
          .where("countryTo", isEqualTo: shipment.countryTo)
          .where("stateFrom", isEqualTo: shipment.stateFrom)
          .where("stateTo", isEqualTo: shipment.stateTo)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          //filter userId (not same user) and date ranges
          var filterdDocs = snapshot.data.documents
              .where(
                (doc) =>
                    doc['userId'] != shipment.userId &&
                    DateFilter.compareDatesWithRangeAndNow(
                      sourceDate: shipment.date,
                      destinationDate:
                          DateTime.parse((doc['date']).toDate().toString()),
                      daysRange: kDatesDaysRange,
                    ),
              )
              .toList();
          if (filterdDocs.length == 0) {
            return Text(
              '0',
              style: TextStyle(fontWeight: FontWeight.bold),
            );
          } else {
            return Row(
              children: <Widget>[
                Text(
                  filterdDocs.length.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                FlatButton(
                  child: Text(
                    'Click to check',
                    style: TextStyle(color: Colors.green),
                  ),
                  onPressed: () async {
                    List<TripData> tripsList = await DatabaseMethods()
                        .getMatchingTripsListFromDocuments(docs: filterdDocs);
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return MatchingTripsScreen(
                        tripsList: tripsList,
                      );
                    }));
                  },
                ),
              ],
            );
          }
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          return Text(
            'Loading matches..',
            style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
          );
        }
      },
    );
  }
}
