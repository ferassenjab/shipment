import 'package:flutter/material.dart';
import 'package:shipment/components/recent_message.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shipment/screens/chat_screen.dart';

final _firestore = Firestore.instance;

class Inbox extends StatefulWidget {
  final String currentUserId, currentUserName;

  Inbox({@required this.currentUserId, @required this.currentUserName});

  @override
  _InboxState createState() => _InboxState();
}

class _InboxState extends State<Inbox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: InboxStream(currentUserId: widget.currentUserId, currentUserName: widget.currentUserName,),
      );
  }
}

class InboxStream extends StatelessWidget {
  final String currentUserId, currentUserName;

  InboxStream({@required this.currentUserId, @required this.currentUserName});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firestore.collection('chat')
          .document('inbox')
          .collection(currentUserId)
          .snapshots(),
      builder: (context, snapshot){
        if(!snapshot.hasData){
          return Column(
                children: <Widget>[
                  SizedBox(height: 60.0,),
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 60.0,
                        width: 60.0,
                      ),
                    ),
                  ),
                ],
              );
        }
        else if(snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        else {  
          print(snapshot.data.documents);
          final messages = snapshot.data.documents;
          List<RecentMessage> recentMessages = [];
          for(var message in messages) {
            final senderName = message.data['contact_name'];
            final messageText = message.data['last_message'];
            final messageTime = message.data['time'];
            final bool sentByMe = message.data['sent_by_me'];
            final bool seen = message.data['seen'];
            recentMessages.add(RecentMessage(
              senderName: senderName,
              messageText: sentByMe ? 'You: ' + messageText : messageText,
              messageTime: messageTime.toString(),
              unread: !seen,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ChatScreen(
                      senderId: currentUserId,
                      receiverId: message.documentID,
                      senderName: currentUserName,
                      receiverName: senderName, //receiver in chat is sender here
                    ))
                );
              },
            ));
          }
          return ListView(
            children: recentMessages,
          );
        }
      },
    );
  }
}